#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Simple SIP registrar
"""

import json
import socketserver
import sys
import time


class SIPRequest():

    def __init__(self, data):
        """Extract from data all relevant items"""

        self.data = data

    def parse(self):
        """Parse request, extracting all its relevant components.

        Extracted values will be variables of the instance"""

        first_nl = self.data.index(b'\n')
        first_line = self.data[:first_nl].decode('utf-8').strip()
        self._parse_command(first_line)
        self._parse_headers(first_nl+1)

    def _get_address(self, uri):
        if uri.startswith('sip:'):
            address = uri[4:]
            schema = 'sip'
        else:
            address = None
            schema = None
        return address, schema

    def _parse_command(self, line):
        """Read command (first line) of request.

        Identify command, uri, result, and address, store them in
        instance variables
        """
        elements = line.split()
        self.command = elements[0]
        self.uri = elements[1]
        if self.command == 'REGISTER':
            self.address, schema = self._get_address(self.uri)
            if schema == 'sip':
                self.result = "200 OK"
            else:
                self.result = "416 Unsupported URI Scheme"
        else:
            self.result = "405 Method Not Allowed"

    def _parse_headers(self, first_nl):

        current_nl = first_nl
        self.headers = {}
        while True:
            next_nl = self.data.index(b'\n', current_nl)
            line = self.data[current_nl:next_nl].decode('utf-8').strip()
            if len(line) == 0:
                break

            elements = line.split(':')
            name = elements[0]
            value = elements[1].lstrip()
            self.headers[name] = value

            current_nl = next_nl


class SIPHandler(socketserver.BaseRequestHandler):
    """
    Simple SIP registrar
    """
    registered = {}

    def process_register(self, request):
        """Process REGISTER command"""
        expires = int(request.headers['Expires'])
        if expires == 0:
            if request.address in self.registered:
                del self.registered[request.address]
        else:
            expire_time = time.time() + expires
            expire_string = time.strftime('%Y-%m-%d %H:%M:%S +0000',
                                          time.gmtime(expire_time))
            self.registered[request.address] = {
                'client': self.client_address[0],
                'expires': expire_string
            }
        self.registered2json()

    def handle(self):
        """
        Handle SIP requests
        """

        data = self.request[0]
        sock = self.request[1]
        sip_request = SIPRequest(data)
        sip_request.parse()
        if (sip_request.command == "REGISTER") and \
                (sip_request.result == "200 OK"):
            self.process_register(sip_request)
        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(),
                    self.client_address)

    @classmethod
    def registered2json(cls):

        with open('registered.json', 'w') as file:
            json.dump(cls.registered, file, indent=4)

    @classmethod
    def json2registered(cls):

        try:
            with open('registered.json', 'r') as file:
                cls.registered = json.load(file)
        except BaseException as exp:
            print("Error reading registered.json, a new onw will be created")
            print(f"  (exception: {exp})")
            cls.registered = {}


def main():
    if len(sys.argv) < 2:
        sys.exit("Usage: python3 server.py <port>")
    port = int(sys.argv[1])

    try:
        serv = socketserver.UDPServer(('', port), SIPHandler)
        print(f"Server listening in port {port}")
    except OSError as e:
        sys.exit(f"Error while trying to listen: {e.args[1]}.")

    SIPHandler.json2registered()

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Server done")
        sys.exit(0)


if __name__ == "__main__":
    main()
