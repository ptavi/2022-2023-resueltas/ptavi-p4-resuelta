#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys


class EchoHandler(socketserver.BaseRequestHandler):
    """
    Echo handler class
    """

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8')
        (sender_ip, sender_port) = self.client_address
        print(f"{sender_ip} {sender_port}", received)
        sock.sendto(received.upper().encode('utf-8'), self.client_address)


def main():
    if len(sys.argv) < 2:
        sys.exit("Usage: python3 server.py <port>")
    port = int(sys.argv[1])
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', port), EchoHandler)
        print(f"Server listening in port {port}")
    except OSError as e:
        sys.exit(f"Error while trying to listen: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Server done")
        sys.exit(0)


if __name__ == "__main__":
    main()
