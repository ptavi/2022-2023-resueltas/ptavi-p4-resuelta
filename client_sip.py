#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Simple client for a SIP registrar
"""

import socket
import sys

usage = "Usage: python3 client.py <ip> <port> register <address> <expiration>"


def main():
    if len(sys.argv) != 6:
        sys.exit(usage)
    sys.argv.pop(0)
    ip = sys.argv.pop(0)
    port = int(sys.argv.pop(0))
    command = sys.argv.pop(0)
    if command == 'register':
        address = sys.argv.pop(0)
        expiration = int(sys.argv.pop(0))
        request = f"REGISTER sip:{address} SIP/2.0\r\n" \
                  + f"Expires: {expiration}\r\n\r\n"
    else:
        sys.exit(usage)

    # Creamos el socket de envío, enviamos el mensaje de registro,
    # y esperamos respuesta.
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.sendto(request.encode('utf-8'), (ip, port))
        data = my_socket.recv(1024)
        print(data.decode('utf-8'))


if __name__ == "__main__":
    main()
